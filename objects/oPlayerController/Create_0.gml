/// @description 

global.playerController = self;

alivePlayerCount = 0;
alivePlayers = ds_list_create();
activePlayerCount = 0;
activePlayers = ds_list_create(); // equal to alivePlayers if in main menu etc.
players = [];

//Create Players
function createPlayerAttributes(color, colorName, buttonLeft, buttonRight, buttonInteract) {
	return {
		color: color,
		colorName: colorName,
		buttonLeft: buttonLeft,
		buttonRight: buttonRight,
		buttonInteract: buttonInteract
	};
}

playerAttributes = [
	// first player is a dummy so refering to player 1 actually gets player 1 (for strings etc)
	createPlayerAttributes(noone,						noone,		noone,		 noone,		 noone),
	createPlayerAttributes(make_color_rgb(255, 255, 0),	"yellow",	vk_left,	 vk_right,	 vk_down),
	createPlayerAttributes(make_color_rgb(0, 255, 255), "torqoise",	ord("A"),	 ord("D"),	 ord("S")),
	createPlayerAttributes(make_color_rgb(0, 255, 0),	"green",	ord("J"),	 ord("L"),	 ord("K")),
	createPlayerAttributes(make_color_rgb(255, 0, 130), "pink",		vk_numpad4,	 vk_numpad6, vk_numpad5),
	createPlayerAttributes(make_color_rgb(255, 102, 0), "orange",	vk_left,	 vk_right,	 vk_down),
	createPlayerAttributes(make_color_rgb(0, 0, 255),	"blue",		vk_left,	 vk_right,	 vk_down),
	createPlayerAttributes(make_color_rgb(255, 0, 0),	"red",		vk_left,	 vk_right,	 vk_down),
	createPlayerAttributes(make_color_rgb(150, 0, 255), "violet",	vk_left,	 vk_right,	 vk_down)
];

for (var i = 1; i <= global.MAX_PLAYER_COUNT; i++) {
	with(instance_create_layer(0, 0, "Players", oPlayer)) {
		playerNumber	= i;
		color			= other.playerAttributes[i].color;
		colorName		= other.playerAttributes[i].colorName;
		buttonLeft		= other.playerAttributes[i].buttonLeft;
		buttonRight		= other.playerAttributes[i].buttonRight;
		buttonInteract	= other.playerAttributes[i].buttonInteract;
		other.players[i] = self;
	}
}

function initPlayers(playerCount) {
	activePlayerCount = playerCount;
	alivePlayerCount = playerCount;
	for(var i = 1; i <= playerCount; i++) {
		with(players[i]) {
			active = true;
			alive = true;
			other.activePlayers[|i-1] = self;
			other.alivePlayers[|i-1] = self;
		}
	}
}

function onPlayerDeath(player) {
	for(var i = 0; i < alivePlayerCount; i++){
		if(alivePlayers[|i] == player){
			player.alive = false;
			ds_list_delete(alivePlayers, i);
			alivePlayerCount--;
			return;
		}
	}
}
