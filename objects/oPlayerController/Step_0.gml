#region Consts
#endregion

#region Variables
#endregion

#region Functions
#endregion

#region Action
	//delete Players on number press
	if (alivePlayerCount <= 1) exit;
	for (var i = 1; i <= 8; i++) {
		if(keyboard_check_pressed(ord(string(i))) && players[i].alive)
			global.gameMode.onPlayerDamage(players[i]);
	}
#endregion
