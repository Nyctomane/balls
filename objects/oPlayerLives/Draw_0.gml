#region Consts
#endregion

#region Variables
#endregion

#region Functions
#endregion

#region Action
	if(!active)
		exit;
		
	// TODO Draw all hears directly from lives array of all players. Positions should've been set in step.
		
	//var lifeStart = player.phiMiddle + ((maxLives-1) * lifeOffset) / 2;
	//var lifePos = lifeStart - (number * lifeOffset);

	for(var i = 0; i < global.playerController.activePlayerCount; i++){
		var player = global.playerController.activePlayers[|i]
		var _lives = _playerLives[player.playerNumber];
		for(var j = 0; j < _lives.maxLives; j++){
			//var life = _lives[i].lifeData[j];
			if(_lives.lifeData[j].active ?? false)
				draw_sprite_ext(sHeart, 0, _lives.position.x + j*_lives.offset, _lives.position.y, 1, 1, 0, player.color, 1);
		}
		
		//draw_sprite_ext(sWall, 0, pos.x, pos.y, 1, 1, phi, player.color, 1);
	}	
#endregion
