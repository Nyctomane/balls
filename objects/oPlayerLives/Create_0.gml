#region Consts
#endregion

#region Variables
	active = true;
	_playerLives = [];
#endregion

#region Functions
	
	function lifeCount(player){
		return global.Utils.countInArray(_playerLives[player.playerNumber].lifeData, function(heart) {return heart.active ?? false;});
	}

	/// Updates the position of hearts for one specific player. 
	/// Radius is 0 per default, meaning hearts should be drawn linearly.
	function update(player, position, rotation, alignment, offset, radius=0) {
		if (radius <= 0) {
			// draw linear
		} else {
			// draw circular
		}
		
		
	}
	
	/// Overwrites/sets hard position for lives of a specific player.
	function setPosition(player, position) {
		
	}
	
	function damagePlayer(player, damage=1) {
		var currentLives = lifeCount(player);
		for(var i = 1; i <= damage; i++){
			var arrayPosition = currentLives - i;
			if(arrayPosition < 0){
				break;
			}
			_playerLives[player.playerNumber].lifeData[arrayPosition].active = false;
		}
		if(lifeCount(player) <= 0){
			global.gameMode.onPlayerZeroLives(player);
		}
	}
#endregion

#region Action
#endregion

