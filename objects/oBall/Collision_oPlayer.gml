if(!other.alive)
	exit;

if(playerCollisionCooldown <= 0){
	global.gameMode.onCollisionBallPlayer(self, other);
	playerCollisionCooldown = playerCollisionCooldownStart;
}