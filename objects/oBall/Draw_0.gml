/*if (rainbow)
	color = make_color_rgb(modulo(r++, 255), modulo(g++, 255), modulo(b++, 255));
*/	
image_blend = color;

var scaleDivisor = 1;
if(size <= 16){
	sprite_index = sBall16;
}
else if(size <= 64){
	sprite_index = sBall64;
	scaleDivisor = 4;
}
else{
	sprite_index = sBall256;
	scaleDivisor = 16;
}
image_xscale = scale / scaleDivisor;
image_yscale = scale / scaleDivisor;

draw_self();