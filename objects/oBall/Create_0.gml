
//Modifiers
curved = false;
rainbow = false;
glitched = false;

//Default values
speed = 1;
direction = 0;
outsideOfCircle = false;
circleCollisionCooldownStart = 20;
circleCollisionCooldown = 0;
playerCollisionCooldownStart = 20;
playerCollisionCooldown = 0;
circleBounceThreshold = 20;
position = getPosition(global.circle);
setPosition(position);
color = c_white;
scale = 1;
size = 16;

//Variables
lastHitBy = pointer_null;

//Functions
function bounceOnCircle(){
	phi = vectorToPhi(position);
	var newDirection = modulo(phi + phi + 180 - direction);
		
	//Set Bounce Angle to Threshold if too small
	if(modulo(phi - newDirection) < (90 + circleBounceThreshold)){
		newDirection = modulo(phi - 90 - circleBounceThreshold);
	}
	else if(modulo(newDirection - phi) < (90 + circleBounceThreshold)){
		newDirection = modulo(phi + 90 + circleBounceThreshold);	
	}
			
	direction = newDirection;
}