#region Consts
#endregion

#region Variables
#endregion

#region Functions
#endregion

#region Action
	if(!outsideOfCircle && vectorDistanceGreater(global.circle.center, position, global.circle.radius + 20)){
		outsideOfCircle = true;
		global.gameMode.onBallLeaveCircle(self);
	}
	
	if(!outsideOfCircle && circleCollisionCooldown <= 0 && !outsideOfCircle && (vectorDistanceGreater(global.circle.center, position, global.circle.radius - size / 2))){
		global.gameMode.onBallCollisionCircle(self);
		circleCollisionCooldown = circleCollisionCooldownStart;
	}
#endregion
