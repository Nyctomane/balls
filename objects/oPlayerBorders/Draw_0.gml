
#region Consts
#endregion

#region Variables
#endregion

#region Functions
#endregion

#region Action
	if(!active)
		exit;

	for(var i = 0; i < global.playerController.alivePlayerCount; i++){
		var player = global.playerController.alivePlayers[|i]
		var playerNumber = player.playerNumber;
		
		var phi = borders[playerNumber].right;
		var pos = circleVector(phi);
		draw_sprite_ext(sWall, 0, pos.x, pos.y, 1, 1, phi, player.color, 1);
		draw_sprite_ext(sWallSeperator, 0, pos.x, pos.y, 1, 1, phi, c_white, 1);
		
		var phi = borders[playerNumber].left;
		var pos = circleVector(phi);
		draw_sprite_ext(sWall, 0, pos.x, pos.y, 1, 1, phi + 180, player.color, 1);
		draw_sprite_ext(sWallSeperator, 0, pos.x, pos.y, 1, 1, phi + 180, c_white, 1);
	}	
#endregion
