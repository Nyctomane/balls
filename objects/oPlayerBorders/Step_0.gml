
#region Consts
#endregion

#region Variables
#endregion

#region Functions
#endregion

#region Action
	//borders[playerNumber].middle	= modulo(prevPhiLeft + sectorWidth / 2);
	
	for(var i = 0; i < global.playerController.alivePlayerCount; i++){
		var playerNumber = global.playerController.alivePlayers[|i].playerNumber;
		var border = borders[playerNumber];
		
		//borders[playerNumber].right = border.rightNew;
		//borders[playerNumber].left = border.leftNew;
		
		//Shift Borders
		if(border.right != border.rightNew){
			borders[playerNumber].right = circleApproach(border.right, border.rightNew, border.rightShiftAmount * shiftSpeed * deltaTime());
		}
		if(border.left != border.leftNew){
			borders[playerNumber].left = circleApproach(border.left, border.leftNew, border.leftShiftAmount * shiftSpeed * deltaTime());
		}
		
		//Update variables
		borders[playerNumber].middle = borders[playerNumber].right + circleDistance(borders[playerNumber].left, borders[playerNumber].right) / 2;
	}
#endregion