
#region Consts
#endregion

#region Variables
	borders = [];
	active = true;
	
	shiftSpeed = 0.3;
#endregion

#region Functions
	/// Returns corrected player movement, depending on current border positions, using player movement data.
	function playerMovement(player, movement) {	
		//Todo: What if movement == 0 and Walls shift?
		if(!active)
			return movement;
		
		var newPlayerPosition = player.phi + movement;
		var playerBorders = borders[player.playerNumber];
		var thresholdLeft = playerBorders.left - player._width/2;
		var thresholdRight =  playerBorders.right + player._width/2;
	
		//Player inside their borders
		if(phiIsInRange(newPlayerPosition, thresholdRight, thresholdLeft))
			return movement;
			
		//Player is outside and closer to left Border
		else if(modulo(newPlayerPosition - thresholdLeft) <= modulo(thresholdRight - newPlayerPosition))
			return modulo(thresholdLeft - player.phi);
			
		//Player is outside and closer to right Border
		else
			return modulo(thresholdRight - player.phi);
		
		/*
		//Player moving left and outside border
		else if(movement > 0)
			return modulo(playerBorders.left - player.phi - player._width/2);
	
		//Player moving right and outside border
		else if(movement < 0)
			return modulo(playerBorders.right - player.phi + player._width/2);*/
	}

	function phiToPlayer(phi){
		for(var i = 0; i < global.playerController.alivePlayerCount; i++){
			var player = global.playerController.alivePlayers[|i];
			if(phiIsInRange(phi, borders[player.playerNumber].right, borders[player.playerNumber].left))
				return player;
		}
		return pointer_null;
	}
	
	function onPlayerDeath(player){
		var alivePlayerCount = global.playerController.alivePlayerCount;
		var newPhiRight = borders[player.playerNumber].middle;
		var sectorWidth = 360 / (alivePlayerCount - 1)
		
		//get list index of dead player
		var listIndexOfDeadPlayer = 0;
		for(var i = 0; i < alivePlayerCount; i++){
			if(global.playerController.alivePlayers[|i] == player){
				listIndexOfDeadPlayer = i;
				break;
			}
		}
		
		//Start updating new borders with player on the right of the dead player
		var listStartIndex = modulo(listIndexOfDeadPlayer + 1, alivePlayerCount); 
		for(var i = listStartIndex; i != listIndexOfDeadPlayer; i = modulo(i + 1, alivePlayerCount)){
			var playerNumber = global.playerController.alivePlayers[|i].playerNumber;
			borders[playerNumber].rightNew = newPhiRight;
			borders[playerNumber].leftNew	= modulo(newPhiRight + sectorWidth);
			borders[playerNumber].rightShiftAmount = circleDistance(newPhiRight, borders[playerNumber].right, true);
			borders[playerNumber].leftShiftAmount	= circleDistance(modulo(newPhiRight + sectorWidth), borders[playerNumber].left, true);
			newPhiRight = modulo(newPhiRight + sectorWidth);
		}
	}
	
	function deleteBorders(){
		active = false;
	}
#endregion

#region Action
#endregion
