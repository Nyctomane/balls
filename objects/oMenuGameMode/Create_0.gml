event_inherited();

//Functions

// one player is active/alive at start by default
global.playerController.initPlayers(7);

global.started = true;

global.gameController.changeGameMode(oPongGameMode);
