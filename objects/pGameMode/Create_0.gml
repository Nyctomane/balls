#region Consts
#endregion

#region Variables
	global.gameMode = self;
	gameInProgress = true;
#endregion

#region Functions
	function standardPlayerMovement(player) {
		var inputs = player.inputs;
		return (inputs.left - inputs.right) * player._speed * deltaTime();
	}

	function executeStandardPlayerMovement(){
		for (var i = 0; i < global.playerController.activePlayerCount; i++) {
			var player = global.playerController.activePlayers[|i];
			player.phi += standardPlayerMovement(player);
		}
	}

	function onPlayerDamage(){}
	function onPlayerDeath(){}
	function checkWinCondition(){}
	function onGameOver(){}
#endregion

#region Action
#endregion