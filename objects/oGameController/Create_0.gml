/// @description 

global.gameController = self;

//Functions

function changeGameMode(newGameMode){
	instance_destroy(global.gameMode);
	global.gameMode = instance_create_layer(0, 0, "Controllers", newGameMode);
}



instance_create_layer(0, 0, "CircleLayer", oCircle);
instance_create_layer(0, 0, "Controllers", oPlayerController);
instance_create_layer(0, 0, "Controllers", oMenuGameMode);