#region Consts
#endregion
#region Variables
#endregion
#region Functions
#endregion
#region Action

	if(!gameInProgress){
		var winner = global.playerController.alivePlayers[|0];
		draw_set_font(fWinning);
		draw_set_halign(fa_middle);
		draw_set_valign(fa_middle);
		draw_set_color(winner.color);
		draw_text(global.circle.x, global.circle.y - 30, winner.colorName +  " WINS!");
		draw_set_font(fWinning2);
		draw_text(global.circle.x, global.circle.y + 30, "Press 'R' to Play Again");
	}
	
#endregion
