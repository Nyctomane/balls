event_inherited()

#region Consts
#endregion

#region Variables
#endregion

#region Functions
#endregion

#region Action
	//PlayerMovement
	for (var i = 0; i < global.playerController.alivePlayerCount; i++) {
		var player = global.playerController.alivePlayers[|i];
		player.phi += playerBorders.playerMovement(player, standardPlayerMovement(player));
	}

	//Ball-Spawn
	if(ballCountdown <= 0){
		spawnBall();
		ballCountdown = BALL_SPAWN_RATE;
	}
	else
		ballCountdown -= deltaTime();
#endregion
