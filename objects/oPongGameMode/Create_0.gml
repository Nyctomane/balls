event_inherited();

#region Consts
	BALL_SPAWN_RATE = 10;
#endregion

#region Variables
	ballCountdown = 0;
	playerBorders = PlayerBorders();
	playerLives = PlayerLives();
#endregion

#region Functions
	function spawnBall(){
		return Ball({_direction : random(360)});
	}

	function onCollisionBallPlayer(ball, player){
		if(!player.active || !player.alive)
			exit;

		var DEFLECT_FACTOR = 5;		//value of direction change relative to where the player hits the ball
		var SPEED_INCREASE = 0.2;	//value to add onto ball speed on collision
	
		var phiPlayer = player.phi;
		var phiBall = vectorDirection(getPosition(global.circle), getPosition(ball));
		ball.direction = modulo(phiPlayer + 180 + DEFLECT_FACTOR * (phiPlayer - phiBall));
	
		ball.speed += SPEED_INCREASE;
		ball.lastHitBy = player;
		ball.color = player.color;
	}
	
	function onBallLeaveCircle(ball){
		var player = playerBorders.phiToPlayer(vectorToPhi(getPosition(ball)));
		if(player != pointer_null){
			ball.color = player.color;
			onPlayerDamage(player);
		}
	}
	
	/// Used to react to positive or negative LP change; displaying healing or damage effects.
	/// Possible to also use with sounds and specific effects for massive changes.
	function onLifePointsChange(player, oldLifePoints, newLifePoints) {
		 // TODO
	}
	
	function onBallCollisionCircle(ball){
		if(!gameInProgress || playerBorders.phiToPlayer(vectorToPhi(getPosition(ball))) == pointer_null){
			with(ball){
				bounceOnCircle();
			}
		}
	}
	
	function checkWinCondition(){
		if(global.playerController.alivePlayerCount == 1)
			return true;
		return false;
	}
	
	function onGameOver(){
		gameInProgress = false;
		playerBorders.deleteBorders();
		
	}
	
	function onPlayerDeath(player){
		playerBorders.onPlayerDeath(player);
		global.playerController.onPlayerDeath(player);
		
		//Check win condition
		if(checkWinCondition()){
			onGameOver();
		}
	}
	
	function onPlayerDamage(player, damage = 1){
		playerLives.damagePlayer(player, damage);
	}
	
	function onPlayerZeroLives(player){
		onPlayerDeath(player);
	}
	
#endregion

#region Action
	clearBallList();
#endregion
