if(!global.started || global.paused || !alive || !active)
	exit;

// check keys
inputs = {
	left: keyboard_check(buttonLeft),
	right: keyboard_check(buttonRight),
	interact: keyboard_check(buttonInteract)
};

