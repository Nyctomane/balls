
randomize();
//random_set_seed(1);

// Consts
global.GAME_WIDTH = 1920;
global.GAME_HEIGHT = 1080;
global.GAME_CENTER = Vector(global.GAME_WIDTH / 2, global.GAME_HEIGHT / 2); // Todo other resolutions??
global.MAX_PLAYER_COUNT = 8;

// Controllers
global.gameController = noone;
global.playerController = noone;
global.gameMode = noone;

// Circle object
global.circle = noone;

// Game Status
global.started = false;
global.paused = false;

//Graphics
//Anti-Aliasing & V-Sync
if(display_aa > 6)
	display_reset(8, true);
else if(display_aa > 2)
	display_reset(4, true);
else if(display_aa > 0)
	display_reset(2, true);
else 
	display_reset(2, true);
	

//DEBUG
/*
show_debug_message(circleDistance(10, 300));
show_debug_message(circleDistance(10, 300, true));
show_debug_message(circleDistance(300, 10));
show_debug_message(circleDistance(300, 10, true));