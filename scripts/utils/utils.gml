global.Utils = {
	countInArray: 
		/* Usage examples:
		1. Counts active elements (provided every element has an "active" attribute):
		countInArray(
			array,
			function(element) {return element.active==true;},
			maxElements
		);
		2. Counts all even elements (crashes if params are not integers etc):
		show_debug_message(countInArray(array, function(element) {return element%2==0;}));
		*/
			
		/// Counts elements in a given array that satisfy a given check function. Default counts all elements.
		function (array, verifier=function(){return true;}, maxIndex=0) {
			var count = 0;
			if(maxIndex==0) {
			    maxIndex = array_length(array);
			}
			for(var i = 0; i < maxIndex; i++) {
			    if(verifier(array[i])) {
			    count++;
			    }
			}
			return count;
		}
}
