
///desc DeltaTime in seconds
function deltaTime(){
	return delta_time / 1000000;
}

//Struct to hold a Vector with 2 coordinates
function Vector(_x, _y) {
	return { 
		x : _x,
		y : _y,
		set : function(_x, _y) {
			x = _x;
			y = _y;
			return self;
		},
		add : function(Vector) {
			x += Vector.x;
			y += Vector.y;
			return self;
		},
		subtract : function(Vector) {
			x -= Vector.x;
			y -= Vector.y;
			return self;
		},
		negate : function() {
			x = -x;
			y = -y;
			return self;
		},
		multiply : function(_scalar) {
			x *= _scalar;
			y *= _scalar;
			return self;
		},
		divide : function(_scalar) {
			x /= _scalar;
			y /= _scalar;
			return self;
		},
		normalize : function() {
			if ((x != 0) || (y != 0)) {
				var _factor = 1/sqrt((x * x) + (y *y));
				x = _factor * x;
				y = _factor * y;	
			}
			return self;
		}, 
		length : function(){
			return sqrt(x * x + y * y);
		}
	};
}

///@param {Vector} start
///@param {Vector} end
function vectorFromVector(_start, _end){
	return Vector(_start.x - _end.x, _start.y - _end.y);
}

function setPosition(position){
	x = position.x;
	y = position.y;
}

function getPosition(object) {
	return Vector(object.x, object.y);
}

///@param {Vector} from
///@param {Vector} to
function vectorDirection(from, to){
	return point_direction(from.x, from.y, to.x, to.y);	
}

///@param {Vector} from
///@param {Vector} to
function vectorDistance(from, to){
	var vec = vectorFromVector(from, to);
	return sqrt(vec.x * vec.x + vec.y * vec.y);
}

///@param {Vector} from
///@param {Vector} to
///@param {real} distance
function vectorDistanceGreater(from, to, distance){
	var vec = vectorFromVector(from, to);
	return (vec.x * vec.x + vec.y * vec.y) > (distance * distance);
}

//get Position from angle and distance from center
function circleVector(phi, radius = global.circle.radius) {
	var circleX = global.circle.x + radius * dcos(phi);
	var circleY = global.circle.y - radius * dsin(phi);
	return Vector(circleX, circleY);
}

function vectorToPhi(vector){
	return vectorDirection(getPosition(global.circle), vector);
}

//modulo as known in standard math (fixes negative values)
function modulo(n, m = 360) {
	return n - floor(n / m) * m;
}

function phiIsInRange(phi, phiRight, phiLeft){
	phi			= modulo(phi);
	phiRight	= modulo(phiRight);
	phiLeft		= modulo(phiLeft);
	
	if(phiRight <= phiLeft)	//Range does not overlap 0
		return (phi >= phiRight and phi <= phiLeft);
	else					//Range overlaps 0
		return (phi >= phiRight or  phi <= phiLeft);
}

function circleDistance(phiLeft, phiRight, ignoreOrder = false) {
	if(!ignoreOrder || modulo(phiLeft - phiRight) <= 180)
		return modulo(phiLeft - phiRight);	
	else
		return modulo(phiLeft - phiRight) - 360;
}

function circleApproach(phiStart, phiEnd, amount) {
	var correctedEnd = phiEnd;
	if(amount > 0){		//Moving left
		if(phiEnd <= phiStart)	//Overlaps 0
			correctedEnd = phiEnd + 360;
		return modulo(min(phiStart + amount, correctedEnd));
	}
	else{					//Moving right
		if(phiStart < phiEnd)	//Overlaps 0
			correctedEnd = phiEnd - 360;
		return modulo(max(phiStart + amount, correctedEnd));
		
	}
}