
//function call: PlayerBorders({_speed : 1, _modifiers : ["rainbow"]});
///@params {struct} params Possible params: _modifiers: string[]
function PlayerBorders(params = {}) {
	var playerBorders = instance_create_layer(0, 0, "CircleAssets", oPlayerBorders);
	with(playerBorders){
		var activePlayerCount = global.playerController.activePlayerCount;
		var activePlayers = global.playerController.activePlayers;
		var sectionWidth = 360 / activePlayerCount;
		for(var i = 0; i < activePlayerCount; i++){
			var temp = {};
			var player				= activePlayers[|i];
			temp.player				= player;
			temp.right				= modulo(i * sectionWidth - 90);
			temp.left				= modulo(temp.right + sectionWidth);
			temp.middle				= modulo(temp.right + sectionWidth / 2);
			temp.rightNew			= temp.right;
			temp.leftNew			= temp.left;
			temp.rightShiftAmount	= 0;
			temp.leftShiftAmount	= 0;
			borders[player.playerNumber] = temp;
			
			activePlayers[|i].phi = temp.middle;
		}
	}
	return playerBorders;
}
