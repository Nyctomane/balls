
//function call: PlayerLives({_maxLives : 2, _modifiers : [""]});
///@params {struct} params Possible params: _modifiers: string[]
function PlayerLives(params = {}) {
	var playerLives = instance_create_layer(0, 0, "CircleAssets", oPlayerLives);
	with(playerLives){
		if (params == pointer_null)
			exit;
		var keys = variable_struct_get_names(params);
		for (var i = 0 ; i < array_length(keys); i++) {
			var key = keys[i]; // example: "_position"
			var value = params[$ key]; // example: "Position(0,0)"
			switch(key) {
				case "_maxLives"	: maxLives = value; break;
				case "_modifiers"	: 
					// value is array of modifier names
					for (var j = 0; j < array_length(value); j++){
						switch(value[j]) {
							case "drawCircular"		: drawCircular = true; break;
							default		: show_error("ERROR: Wrong modifier name", true);
						}
					}
					break;
				default : show_error("ERROR: Wrong parameter name", true);
			}
		}
		
		var activePlayerCount = global.playerController.activePlayerCount;
		var activePlayers = global.playerController.activePlayers;
		for(var i = 0; i < activePlayerCount; i++){
			var temp = {};
			var player			= activePlayers[|i];
			temp.player			= player;
			temp.maxLives		= 3;
			temp.position		= Vector(20, 60*player.playerNumber);
			temp.rotation		= 0;
			temp.alignment		= "Left";
			temp.offset			= 40;
			temp.radius			= 0;
			for(var j = 0; j < temp.maxLives; j++){
				temp.lifeData[j] = {
					active: true,
					position: pointer_null
					//"isHoly": false
				};
			}
			_playerLives[player.playerNumber] = temp;
		}
	}
	return playerLives;
}

/*
// _playerLives = 
[
	{ // for each player
		"player": Player,
		"maxLives": int,
		"position": Position,
		"rotation": float,
		"alignment": Enum(left, center, right),
		"offset": float,
		"radius": float,	 // (linear if <= 0)
		// ...
		"lifeData": [
			{ // for each life point, basically
				"active": boolean,
				"position": Position,
				//"isHoly": true,  			//checked in draw to assign sprite, color, size, ...
				""
				// ...
			}
		]
	}
]
*/