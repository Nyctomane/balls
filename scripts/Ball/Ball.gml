
//function call: Ball({_speed : 1, _modifiers : ["rainbow"]});
///@params {struct} params Possible params: _position : Position; _direction : real; _speed : real; _modifiers: string[]
function Ball(params = {}) {
	
	var ball = instance_create_layer(0, 0, "Instances", oBall);
	with(ball){
		
		if (params == pointer_null)
			exit;
		var keys = variable_struct_get_names(params);
		for (var i = 0 ; i < array_length(keys); i++) {
			var key = keys[i]; // example: "_position"
			var value = params[$ key]; // example: "Position(0,0)"
			switch(key) {
				case "_position"	: position = value; break;
				case "_speed"		: speed = value; break;
				case "_direction"	: direction = value; break;
				case "_modifiers"	: 
					// value is array of modifier names
					for (var j = 0; j < array_length(value); j++){
						switch(value[j]) {
							case "curved"			: curved = true; break;
							case "glitched"			: glitched = true; break;
							case "rainbow"			: rainbow = true; break;
							default					: show_error("ERROR: Wrong modifier name", true);
						}
					}
					break;
				default : show_error("ERROR: Wrong parameter name", true);
			}
		}
	}
	ds_list_add(getBallList(), ball);
	return ball;
}

function getBallList(){
	//List of all existing Balls
	static ballList = ds_list_create();
	return ballList;
}

function clearBallList(){
	ds_list_clear(getBallList());
}